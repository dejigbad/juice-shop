import sys
import requests

if len(sys.argv) < 2:
    print("Usage: python script.py <file_name>")
    sys.exit(1)

file_name = sys.argv[1]
scan_type = ''

if file_name == 'gitleaks.json':
    scan_type = 'Gitleaks Scan'
elif file_name == 'njsscan.sarif':
    scan_type = 'SARIF'
elif file_name == 'semgrep.json':
    scan_type = 'Semgrep JSON Report'
elif file_name == 'retire.json':
    scan_type = 'Retire.js Scan'
else:
    print("Invalid file name")
    sys.exit(1)

# Define the headers with the API token
headers = {
    'Authorization': 'Token 548afd6fab3bea9794a41b31da0e9404f733e222'
}

# Define the URL for importing scan results
url = 'https://demo.defectdojo.org/api/v2/import-scan/'

# Define the data to be sent in the request
data = {
    'active': True,
    'verified': True,
    'scan_type': scan_type,
    'minimum_severity': 'Low',
    'engagement': 14
}

# Open file in binary mode
with open(file_name, 'rb') as file:
    # Define the file to be uploaded
    files = {'file': file}

    # Make a POST request to import the scan results
    response = requests.post(url, headers=headers, data=data, files=files)

# Check if the request was successful (status code 201)
if response.status_code == 201:
    print('Scan results imported successfully')
else:
    # If the request failed, print the error message
    print(f'Failed to import scan results: {response.text}')
